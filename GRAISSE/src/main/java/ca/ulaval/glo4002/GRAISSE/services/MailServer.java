package ca.ulaval.glo4002.GRAISSE.services;

public interface MailServer {
	
	public void sendMail(Mail mail);
}