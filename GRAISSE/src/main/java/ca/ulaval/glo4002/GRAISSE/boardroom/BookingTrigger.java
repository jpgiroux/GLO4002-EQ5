package ca.ulaval.glo4002.GRAISSE.boardroom;

import ca.ulaval.glo4002.GRAISSE.trigger.Trigger;

public interface BookingTrigger extends Trigger<BookingAssignable> {
	
}
