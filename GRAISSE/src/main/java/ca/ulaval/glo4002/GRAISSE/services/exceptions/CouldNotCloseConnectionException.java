package ca.ulaval.glo4002.GRAISSE.services.exceptions;

public class CouldNotCloseConnectionException extends RuntimeException {

	private static final long serialVersionUID = 794627584012534592L;
}
