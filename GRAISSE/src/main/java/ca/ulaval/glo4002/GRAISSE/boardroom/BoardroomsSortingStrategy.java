package ca.ulaval.glo4002.GRAISSE.boardroom;

import java.util.Collection;

public interface BoardroomsSortingStrategy {
	
	public Collection<Boardroom> sort(Collection<Boardroom> boardrooms);	
}
