package ca.ulaval.glo4002.GRAISSE.services.exceptions;

public class CouldNotSendMailException extends RuntimeException {
	
	private static final long serialVersionUID = 293081653311135869L;
}