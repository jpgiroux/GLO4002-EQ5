package ca.ulaval.glo4002.GRAISSE.user.exceptions;

public class UserNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = -2816602123028033531L;
}