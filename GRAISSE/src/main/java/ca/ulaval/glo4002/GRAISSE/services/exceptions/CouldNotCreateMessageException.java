package ca.ulaval.glo4002.GRAISSE.services.exceptions;

public class CouldNotCreateMessageException extends RuntimeException {
	
	private static final long serialVersionUID = -3931305368588474694L;
}
