package ca.ulaval.glo4002.GRAISSE.boardroom.exceptions;

public class UnableToAssignBookingException extends RuntimeException {

	private static final long serialVersionUID = -2033487441385824286L;
}
