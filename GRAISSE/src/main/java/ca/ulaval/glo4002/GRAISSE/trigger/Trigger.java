package ca.ulaval.glo4002.GRAISSE.trigger;

public interface Trigger<T> {
	
	public void update(T object);
}