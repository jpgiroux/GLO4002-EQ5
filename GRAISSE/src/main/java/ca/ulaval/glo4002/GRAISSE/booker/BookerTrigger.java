package ca.ulaval.glo4002.GRAISSE.booker;

import ca.ulaval.glo4002.GRAISSE.trigger.Trigger;

public interface BookerTrigger extends Trigger<Booker> {
	
}