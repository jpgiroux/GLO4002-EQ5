package ca.ulaval.glo4002.GRAISSE.services;

public interface MailSender {
	
	public void send(Mail mail);
}
